import 'package:flutter/material.dart';

const cvMainColor = const Color(0xFF212156);
const cvLight = const Color(0xFFA874FF);
const cvDark = const Color(0xFF6D57C3);