import 'utils.dart';
import 'package:flutter/material.dart';

class CvTheme {
  static get theme {
    return ThemeData(
        primaryColor: cvMainColor,
        pageTransitionsTheme: PageTransitionsTheme());
  }
}
